/*
 * SHEIKH   Rakib   11502605 - Université Paris 13 - Institut Galilée
 * TD/TP 2 - Algo   Pivot de gauss
 * http://noobzik.ml
 */

#include <stdio.h>  /* printf && scanf */
#include <stdlib.h> /* EXIT_SUCCESS; */
#define N 3         /* Matrice Carre */
#define NB_MAX 1
/* Prototype des fonctions */

/*
 * Cette fonction permet de mettre a jour la colonne du pivot
 * On peut penser qu'il prends en entier une matrice
 */
int colonne_pivot(float *M, int l, int c, int lignecourrant, int colonnepivot);
void descente(float *M, int l, int c);

/*
 * Cette fonction doit surement chercher quel est le pivot a utiliser
 * Forcément, il revoie un entier qui est le numéro de la ligne
 */
int pivot(float *M, int l, int c, int lignecourrant, int colonnepivot); // Fait

/*
 * Pour les trois fonctions, il est évident qu'on va rien renvoyer
 * On taf directectment avec les pointeurs
 */
void annule(float *M, int l, int c, int lignecourrant, int colonnepivot);
void echange(float *M, int l, int c, int lignecourrant, int lignepivot);

void afficher_matrice(float *M);

/* Fonctions de matrices */

/*
 * creer_matrice retourne une matrice avec de la mémoire alloué
 */
float *creer_matrice();

/*
 * liberer_mémoire libere la mémoire alloué
 */
void liberer_memoire(float *M);
/*----------------------------------------------------------------------------*/
/* Fonction principale */

int main() {

  /* Déclarations des variables */

  float *A = creer_matrice();

  A[0 * N + 0] = 3.0;
  A[0 * N + 1] = 2.0;
  A[0 * N + 2] = 1.0;
  A[1 * N + 0] = 0.0;
  A[1 * N + 1] = 0.0;
  A[1 * N + 2] = 2.0;
  A[2 * N + 0] = 6.0;
  A[2 * N + 1] = 4.0;
  A[2 * N + 2] = -1.0;

  /* Début du programme principale */
  afficher_matrice(A);
  descente(A, 3, 3);
  liberer_memoire(A);

  /* Valeur de fonction */

  return EXIT_SUCCESS;
}
/*---------------------------------------------------------------------------*/

/*
 * Fait
 */
int pivot(float *M, int l, int c, int lignecourrant, int colonnepivot) {
  int i;
  int p = lignecourrant;

  for (i = lignecourrant + 1; i < l; i++) {
    if ((M[c * i + colonnepivot] > M[c * p + colonnepivot] &&
         M[c * p * colonnepivot > 0]) ||
        (M[c * i + colonnepivot] && M[c * p + colonnepivot]))
      p = i;
  }
  return p;
}

/*
 * Fait
 */

void echange(float *M, int l, int c, int lignecourrant, int lignepivot) {
  int j;
  float tmp;
  for (j = 0; j < c; j++) {
    tmp = M[c * lignecourrant + j];
    M[c * lignepivot + j] = tmp;
  }
}

/*
 *
 */

void annule(float *M, int l, int c, int lignecourrant, int colonnepivot) {
  int i, j;
  for (i = lignecourrant + 1; i < l; i++) {
    float r = M[c * i + colonnepivot] / M[i * lignecourrant + colonnepivot];
    M[c * i + colonnepivot] = 0;
    for (j = colonnepivot + 1; j < c; j++)
      M[c * i + j] = M[c * i + j] - r * M[c * lignecourrant + j];
  }
}
/*
 * Work in progess...
 *
 * return -1
 */
int colonne_pivot(float *M, int l, int c, int lignecourrant, int colonnepivot) {
  int i = lignecourrant;

  if (colonnepivot == c)
    return 0;
  else {
    while (M[c * i + colonnepivot] == 0 && i < l - 1)
      i++;
    if (M[c * i + colonnepivot] != 0)
      return colonnepivot;
    else
      return colonne_pivot(M, l, c, lignecourrant, colonnepivot + 1);
  }
}

/*
 *
 */

void descente(float *M, int l, int c) {
  int lignecourrant = 0;
  while (lignecourrant < l) {
    if (colonne_pivot(M, l, c, 0, 0) != 0 && pivot(M, l, c, 0, 0) != 0) {
      echange(M, l, c, 0, 0);
      annule(M, l, c, 0, 0);
    }
  }
}

float *creer_matrice() {
  float *A = (float *)(malloc(N * N * sizeof(float)));
  return A;
}

void afficher_matrice(float *M) {
  int i, j;

  for (i = 0; i < N; i++) {
    printf("|");
    for (j = 0; j < N; j++)
      printf("%.1f\t", M[i * N + j]);
    printf("|\n");
  }
}

void liberer_memoire(float *M) { free(M); }
