#ifndef MATRICES_STRASSEN
#define MATRICES_STRASSEN

/* Prototype */

float * creer_matrice             (int);
void    liberer_memoire           (float * M);

void    inic_matrice              (float *M, int);                              /* Ã  faire */
void    afficher_matrice          (float *M, int);                              /* Ã  faire */
float * sous_matrice              (float * M, int, int, int, int);              /* Ã  faire */
void    addition_matrices         (float * X, float *Y, float * R, int);        /* Ã  faire */
void    soustraction_matrices     (float * X, float *Y, float * R, int);        /* Ã  faire */
void    composer_matrices         (float * C, int, float *T1, int, int, int);   /* Ã  faire */

float * strassen                  (float * A, float * B, int);

#endif //MATRICES_STRASSEN
