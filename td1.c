#include <stdio.h>  /* printf && scanf */
#include <stdlib.h> /* EXIT_SUCCESS; */
#include <time.h>   /* SRAND */
#define NB_MAX 10   /* Aleatoire max */
#define N 4         /* Taille matrice (carré) */

/* Declarations des fonctions */
float *creer_matrice();
float *creer_matrice_hilbert();
float *creer_matrice_stochastique();
float *creer_matrice_circulante();
float *dilation(float *M, float s);
float *addition_matrices(float *A, float *B);
float *evalutation_matrice(float *A, float *X);
float *prod_matrice(float *A, float *B);
void afficher_matrice(float *M);
void afficher_vecteur(float *Y);
void init_matrice(float *A);
void liberer_memoire(float *M);

/* Fonction principale */

int main() {

  /* Déclarations des variables */

  float *hilbert = creer_matrice_hilbert();
  float *stochastique = creer_matrice_stochastique();
  float *circulante = creer_matrice_circulante();

  float *A;
  float *B;
  float *C;
  float *D;
  float *E;
  float *X;

  A = creer_matrice();
  B = creer_matrice();
  X = (float *)malloc(N * sizeof(float));

  /* remplisage de A */

  A[0 * N + 0] = 2.0;
  A[0 * N + 1] = 3.0;
  A[0 * N + 2] = 1.0;
  A[0 * N + 3] = 4.0;
  A[1 * N + 0] = 2.0;
  A[1 * N + 1] = 6.0;
  A[1 * N + 2] = 1.0;
  A[1 * N + 3] = 7.0;
  A[2 * N + 0] = 4.0;
  A[2 * N + 1] = 8.0;
  A[2 * N + 2] = 1.0;
  A[2 * N + 3] = 9.0;
  A[3 * N + 0] = 9.0;
  A[3 * N + 1] = 6.0;
  A[3 * N + 2] = 1.0;
  A[3 * N + 3] = 8.0;

  /* remplisage de B */

  B[0 * N + 0] = 1.0;
  B[0 * N + 1] = 2.0;
  B[0 * N + 2] = 1.0;
  B[0 * N + 3] = 2.0;
  B[1 * N + 0] = 3.0;
  B[1 * N + 1] = 1.0;
  B[1 * N + 2] = 2.0;
  B[1 * N + 3] = 1.0;
  B[2 * N + 0] = 1.0;
  B[2 * N + 1] = 1.0;
  B[2 * N + 2] = 1.0;
  B[2 * N + 3] = 1.0;
  B[3 * N + 0] = 4.0;
  B[3 * N + 1] = 3.0;
  B[3 * N + 2] = 3.0;
  B[3 * N + 3] = 4.0;

  /* Remplisage de E */

  X[0] = 5.0;
  X[1] = 7.0;
  X[2] = 2.0;
  X[3] = 3.0;

  /* Début du programme principale */

  printf("Matrice de Hilbert\n");
  afficher_matrice(hilbert);

  printf("\nMatrice Stochastique\n");
  afficher_matrice(stochastique);
  printf("\n");

  printf("Matrice circulante\n");
  afficher_matrice(circulante);
  printf("\n");

  C = prod_matrice(A, B);

  printf("La matrice A\n");
  afficher_matrice(A);

  printf("La matrice B\n");
  afficher_matrice(B);

  printf("Multiplication de matrice A et B\n");
  afficher_matrice(C);

  printf("\nDilation 2 de la matrice A\n");
  dilation(A, 2);
  afficher_matrice(A);

  printf("Addition 2 matrices A et B\n");
  D = addition_matrices(A, B);
  afficher_matrice(D);

  printf("Matrice X :\n");
  afficher_vecteur(X);
  printf("Evalutation Matrice de A et X\n");
  E = evalutation_matrice(A, X);
  afficher_vecteur(E);

  // On libère les mémoires allouées

  liberer_memoire(X);
  liberer_memoire(E);
  liberer_memoire(D);
  liberer_memoire(C);
  liberer_memoire(A);
  liberer_memoire(B);
  liberer_memoire(circulante);
  liberer_memoire(stochastique);
  liberer_memoire(hilbert);

  /* Valeur de fonction */

  return EXIT_SUCCESS;
}

/*
 * Créer une matrice en fonction de sa taille et lui alloue de la mémoire
 * Retourne une matrice vide flottante
 */
float *creer_matrice() {
  float *M = (float *)malloc(N * N * sizeof(float));
  return M;
}

/*
 * Genere une matrice de Hilbert
 * Retourne une matrice flottante
 */
float *creer_matrice_hilbert() {

  float *M = (float *)malloc(N * N * sizeof(float));
  int i, j;

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      M[i * N + j] = 1 / (float)(1 + i + j);
    }
  }
  return M;
}

/*
 * Génère une matrice aléatoire entre 0 et 9
 * Retourne une matrice flottante
 */
float *creer_matrice_stochastique() {

  float *M = (float *)malloc(N * N * sizeof(float));
  int i, j;

  srand(time(NULL));

  /* Generation de la premiere ligne */

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      M[i * N + j] = (float)(rand() % (NB_MAX));
    }
  }
  return M;
}

/*
 * Génère une ligne de matrice puis fait permuter chaque coef de maniere
 * circulaire
 * Retourne une matrice flottante
 */
float *creer_matrice_circulante() {

  float *M = (float *)malloc(N * N * sizeof(float));
  float tmp;
  int i, j;

  for (j = 0; j < N; j++) {
    M[0 * N + j] = (float)(rand() % (NB_MAX));
  }

  /* Switching circulante */

  j = 0;

  for (i = 1; i < N; i++) {
    tmp = M[(i - 1) * N];
    for (j = 0; j < N; j++) {
      M[i * N + j] = M[(i - 1) * N + (j + 1)];
    }
    M[i * N + (j - 1)] = tmp;
  }

  return M;
}

/*
 * Multiplication de la matrice A par un scalaire
 * Retourne son résultat sous forme de matrice flottante
 */
float *dilation(float *M, float s) {
  int i, j;

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      M[i * N + j] = s * M[i * N + j];
    }
  }
  return M;
}

/*
 * Addition de la Matrice A et B
 * Retourne son résultat sous forme de matrice flottante
 */
float *addition_matrices(float *A, float *B) {
  int i, j;
  float *C = creer_matrice();
  init_matrice(C);

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      C[i * N + j] = A[i * N + j] + B[i * N + j];
    }
  }
  return C;
}

/*
 * Multiplication de la matrice A et B
 * Retourne son résultat sous forme de matrice flottante
 */
float *prod_matrice(float *A, float *B) {
  int i, j, k = 0;
  float *C = creer_matrice();
  init_matrice(C);

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      for (k = 0; k < N; k++) {
        C[i * N + j] = C[i * N + j] + A[i * N + k] * B[k * N + j];
      }
    }
  }
  return C;
}

/*
 * Multiplication de la matrice par un vecteur
 * Retourne un vecteur flottante
 */
float *evalutation_matrice(float *A, float *X) {
  int i, j;
  float *Y = (float *)malloc(N * sizeof(float));

  for (i = 0; i < N; i++) {
    Y[i] = 0;
    for (j = 0; j < N; j++) {
      Y[i] = Y[i] + A[i * N + j] * X[j];
    }
  }
  return Y;
}

/*
 * Affiche la matrice sur le terminal
 */
void afficher_matrice(float *M) {
  int i, j;

  for (i = 0; i < N; i++) {
    printf("|");
    for (j = 0; j < N; j++)
      printf("%.1f\t", M[i * N + j]);
    printf("|\n");
  }
}

/*
 * Affiche le contenu du vecteur
 */
void afficher_vecteur(float *Y) {
  int i;

  for (i = 0; i < N; i++) {
    printf("|%.1f\t|\n", Y[i]);
  }
}

/*
 * Initialise tout les coefficients de la matrice à 0
 */
void init_matrice(float *M) {
  int i, j;

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      M[i * N + j] = 0.0;
    }
  }
}

/* Ca a été auto indenté par clang.format car cette fonction comporte juste une
 * instruction !
 *
 *Permet de liberer de la mémoire allouée
 */
void liberer_memoire(float *M) { free(M); }
